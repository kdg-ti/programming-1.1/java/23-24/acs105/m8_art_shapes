package tr01_interfaces.products;

/**
 * @author Christian Cambier
 * @version 1.0     09/2022
 */
public class Camera extends Product {
    /*
      * Instance data member(s)
     */
    private int pixels;
    
    /*
      Constructor(s)
     */
    public Camera(String description, int price) {
        this(null, description, price, 0);
    }  // ctor
    
    public Camera(String code, String description, double price) {
        this(code, description, price, 0);
    }  // ctor
    
    public Camera(String code, String description, double price, int pixels) {
        super(code, description, price);
        setPixels(pixels);
    }  // ctor
    
    /*
      Getter(s)/Setter(s)
    */
    public int getPixels() {
        return pixels;
    }
    
    public final void setPixels(int pixels) {
        this.pixels = pixels;
    }
    
    /*
       Method(s)
     */
    @Override
    public String toString() {
        return String.format("%s pixels:%d ", super.toString(), getPixels());
    }  // toString()
    
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Camera)) {
            return false;
        }
        if (!super.equals(other)) {
            return false;
        }
        
        Camera camera = (Camera) other;
        return pixels == camera.pixels;
    }  // equals()
    
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + pixels;
        return result;
    }  // hashCode()
    
    @Override
    public double getVat() {
        return 0.21;
    }  // getVat()
}  // class