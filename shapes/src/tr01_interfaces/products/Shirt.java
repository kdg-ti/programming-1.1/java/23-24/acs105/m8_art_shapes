package tr01_interfaces.products;

import museum.Art;
import museum.Colour;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Christian Cambier
 * @version 1.0     09/2022
 */
public class Shirt extends Product implements Art {
    /*
      * Instance data member(s)
     */
    private String size;
    private String gender;
    private Colour colour = Colour.getRandomColour();
    
    /*
      Constructor(s)
     */
    public Shirt( String description, int price) {
        this(null, description, price, null, null);
    }  // ctor
    
    public Shirt(String code, String description, double price) {
        this(code, description, price, "", "");
    }  // ctor
    
    public Shirt(String code, String description, double price, String size, String gender) {
        super(code, description, price);
        setSize(size);
        setGender(gender);
    }  // ctor
    
    /*
      Getter(s)/Setter(s)
    */
    public String getSize() {
        return size;
    }
    
    public final void setSize(String size) {
        this.size = size;
    }
    
    public String getGender() {
        return gender;
    }
    
    public final void setGender(String gender) {
        this.gender = gender;
    }
    
    /*
       Method(s)
     */
    @Override
    public String toString() {
        return String.format("%s size:%-15s  gender:%s colour:%s", super.toString(), size, gender,colour);
    }  // toString()
    
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        if (!super.equals(other)) {
            return false;
        }
        
        Shirt shirt = (Shirt) other;
        
        if (!Objects.equals(size, shirt.size)) {
            return false;
        }
        return Objects.equals(gender, shirt.gender);
    }
    
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (size != null ? size.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        return result;
    }
    
    @Override
    public double getVat() {
        return 0.21;
    }  // getVat()

    @Override
    public void paint(Colour colour) {
        this.colour=colour;
    }
}  // class
