package tr01_interfaces.products;

import java.util.Objects;

/**
 * @author Christian Cambier
 * @version 1.0     09/2022
 */
public class Book extends Product {
    /*
      * Instance data member(s)
     */
    private String author;
    private String title;
    
    /*
      Constructor(s)
     */
    public Book(String title, int price) {
        this(null, null, price, null, title);
    }  // ctor
    
    public Book(String code, String description, double price) {
        this(code, description, price, null, null);
    }  // ctor
    
    public Book(String code, String description, double price, String author, String title) {
        super(code, description, price);
        setAuthor(author);
        setTitle(title);
    }  // ctor
    
    /*
      Getter(s)/Setter(s)
     */
    public String getAuthor() {
        return author;
    }
    
    public final void setAuthor(String author) {
        this.author = author;
    }
    
    public String getTitle() {
        return title;
    }
    
    public final void setTitle(String title) {
        this.title = title;
    }
    
    /*
       Method(s)
     */
    @Override
    public String toString() {
        return String.format("%s author:%-15s  title:%-30s", super.toString(), author, title);
    }  // toString()
    
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        
        if (!super.equals(other)) {
            return false;
        }
        
        Book book = (Book) other;
        
        if (!Objects.equals(author, book.author)) {
            return false;
        }
        return Objects.equals(title, book.title);
    }
    
    @Override
    public int hashCode() {
        int result = author != null ? author.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        return result;
    }
    
    @Override
    public double getVat() {
        return 0.06;
    }  // getVat()
}  // class