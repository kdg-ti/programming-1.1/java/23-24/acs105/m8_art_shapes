package be.kdg.java.exercises.shapes.tr01_interfaces.client;

import be.kdg.java.exercises.shapes.tr01_interfaces.graphics.*;
import museum.Art;
import museum.Colour;
import tr01_interfaces.products.Shirt;

import java.util.Arrays;

import static museum.Colour.*;

/**
 * @author Christian Cambier
 * @version 1.0     09/2022
 */
public class InterfacesApp {
    
    public static void main(String[] args) {
        System.out.println("ex01_interfaces");
        System.out.println("--------------- \n");
        
        final int DOUBLE = 2;
        
        Shape[] shapes = new Shape[5];
      //  shapes[0] = new Rectangle(4, 5, 10, 20,new Colour(255,0,0));
        shapes[0] = new Rectangle(4, 5, 10, 20, RED);
        shapes[1] = new Circle(4, 5, 10);
        shapes[2] = new Circle(7, 8, 20);
        
        for (Shape shape : shapes) {
            if (shape != null) {
                System.out.printf("Before scale(%d) \n", DOUBLE);
                System.out.printf("\t%s\n", shape);
                // shape.scale(DOUBLE);
                System.out.printf("After scale(%d) \n", DOUBLE);
                System.out.printf("\t%s\n\n", shape);
            }
        }
        Shirt race = new Shirt("The infinite race", 20);
        System.out.println(race);

        Art[] works = {shapes[0],shapes[1],race};
        for (Art work :works){
            work.paint(BLACK);
        }
        System.out.println(">>> Paint it black");
        System.out.println(Arrays.toString(works));

    }  // main()
    
}  // class
