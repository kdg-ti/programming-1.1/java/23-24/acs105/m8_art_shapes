package be.kdg.java.exercises.shapes.tr01_interfaces.client;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionDemo {
	public static void main(String[] args) {
		int favourite;
		try {
			favourite = getFavourite();
		} catch (InputMismatchException e) {
			System.err.println("Error reading number, assigning 42 " + e);
			favourite = 42;
		} catch (Exception e) {
			System.err.println("This is hopeless, you get a 0 " + e);
			favourite = 0;
		}
		System.out.println("Your lucky number is " + favourite);
	}

	private static int getFavourite() throws Exception {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please enter your favourite number: ");
		return scanner.nextInt();
	}
}
