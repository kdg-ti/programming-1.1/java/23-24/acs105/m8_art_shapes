package museum;

import java.util.Random;

public class Colour {
	private int red;
	private int blue;
	private int green;
	private static Random random = new Random();
	private static int MAX_COLOR = 255;
	public static final Colour BLACK = new Colour(0,0,0);
	public static final Colour RED = new Colour(255,0,0);

	public Colour(int red, int blue, int green) {
		this.red = red;
		this.blue = blue;
		this.green = green;
	}

	public int getRed() {
		return red;
	}

	public void setRed(int red) {
		this.red = red;
	}

	public int getBlue() {
		return blue;
	}

	public void setBlue(int blue) {
		this.blue = blue;
	}

	public int getGreen() {
		return green;
	}

	public void setGreen(int green) {
		this.green = green;
	}

	public static  Colour getRandomColour() {
		return new Colour(random.nextInt(MAX_COLOR+1),
			random.nextInt(MAX_COLOR+1),
			random.nextInt(MAX_COLOR+1));
	}

	@Override
	public String toString() {
		return "Colour{" +
			"red=" + red +
			", blue=" + blue +
			", green=" + green +
			'}';
	}
}
