package museum;

public interface Art {
	void paint(Colour colour);
}
